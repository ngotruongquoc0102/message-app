const path = require('path');
require('dotenv').config();

const BASE_PATH = path.join(__dirname, 'src', 'database');

const config = {
  client: 'postgresql',
  connection: {
    database: process.env.DB_DATABASE,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
  },
  pool: {
    min: 2,
    max: 10,
  },
  migrations: {
    directory: path.join(BASE_PATH, 'migrations'),
    tableName: 'migrations',
  },
  seeds: {
    directory: path.join(BASE_PATH, 'seeders'),
  },
};
module.exports = {
  development: config,
};
