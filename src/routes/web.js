import express from 'express';
import userAdminRouter from '../App/User/routesAuth';
import uploadImage from '../App/upload/routes';
import messages from '../App/Messages/routes';

const router = express.Router();

router.use('/', userAdminRouter);
router.use('/', uploadImage);
router.use('/', messages);
export default router;
