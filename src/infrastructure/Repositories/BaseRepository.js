/* eslint-disable lines-between-class-members */

import knex from '../../config/database';

class BaseRepository {
    constructor() {
        this.getTable = this.getTableName();
        this.getTableFriend = this.getTableFriend();
    }

    cloneQuery() {
        return knex(this.getTable).clone();
    }

    cloneQueryFriend() {
        return knex(this.getTableFriend).clone();
    }
    getBy(clauses = {}, columns = ['*']) {
        console.log(clauses);
        return this.cloneQuery().where(clauses).select(columns).first();
    }

    insertData(data) {
        return this.cloneQuery().insert(data);
    }

    createFriend(data) {
        return this.cloneQueryFriend().insert(data);
    }
    update(clauses, attributes, returning = ['*']) {
        return this.cloneQuery().where(clauses).update(attributes).returning(returning);
    }

    getByFriend(clause = {}, columns = ['*']) {
        return this.cloneQueryFriend().where(clause).select(columns).first();
    }

    listFriends(clause = {}, columns = ['*']) {
        return this.cloneQueryFriend().innerJoin('users', 'users.id', 'friends.friendId').where(clause).select(columns);
    }

    updateFriend(clauses, attributes, returning = ['*']) {
        return this.cloneQueryFriend().where(clauses).update(attributes).returning(returning);
    }

    deleteFriend(clause) {
        return this.cloneQueryFriend().where(clause).delete();
    }
}

export default BaseRepository;
