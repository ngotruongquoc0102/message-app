class BaseAuthorize {
    constructor() {
        this.defineRoutes();
    }

    defineRoutes() {
        this.route = [];
    }

        handleAuthorization(route) {
        return async (req, res, next) => {
          try {
            let method = this.routes[route];
            method = this[`authorize${method}`];
            if (!method) {
              return next();
            }
    
            return await method.bind(this)(req, res, next);
          } catch (e) {
            /* TODO: Change to render view */
            console.log(e.stack);
            // return res.json(ResponseHelper.error(e.message, e.code)).status(e.code);
          }
        };
      }
}
export default BaseAuthorize;
