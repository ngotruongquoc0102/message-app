module.exports = {
    flash_middleware: (req, res, next) => {
        res.locals.conversationId = req.session.conversationId;
        next();
    },
}