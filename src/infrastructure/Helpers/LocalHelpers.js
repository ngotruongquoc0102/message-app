module.exports = (res) => {
    res.locals.handlerErrors = (key) => {
        const errors = res.locals.messages;
        let error = '';
        if (errors) {
                Object.keys(errors).forEach((item) => {
                    errors[item].forEach((element) => {
                        if (element.param === key) {
                           error = element.msg;
                        }
                    });
                });
            return error;
        }
    };
    res.locals.getValueBack = (key) => {
       const valueError = res.locals.oldValue;
       if (valueError) {
        return valueError[key];
    }
    };

    res.locals.getActiveConversation = (conversationActive) => {
        const conversationId  = res.locals.conversationId;
        if (conversationActive === conversationId) {
           return 'activeConversation';
        }
     return '';
    }
};
