
class BaseController {
 callMethod(method) {
     return async (req, res, next) => {
        // this.cUser = req.session.cUser;
        try {
            return await this[method](req, res, next);
        } catch (error) {
            return next(error);
        }
     };
 }
}
export default BaseController;
