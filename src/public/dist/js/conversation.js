
 
$(function () {
    /**
     * 
     * Some examples of how to use features.
     *
     **/
  
    $(document).on('load', '.list-group-item', function() {
        console.log('hello');
    })
    let className = $('.list-group-item[data-className="conversationActive"]');
    className.addClass('activeConversation');
    var conversationId;
    var chat = {
        Message: {
            add: function (message, type) {

                var chat_body = $('.layout .content .chat .chat-body');
                if (chat_body.length > 0) {

                    type = type ? type : '';
                    message = message ? message : 'Lorem ipsum dolor sit amet.';

                    $('.layout .content .chat .chat-body .messages').append('<div class="message-item ' + type + '"><div class="message-content">' + message + '</div><div class="message-action">PM 14:25 ' + (type ? '<i class="ti-check"></i>' : '') + '</div></div>');

                    chat_body.scrollTop(chat_body.get(0).scrollHeight, -1).niceScroll({
                        cursorcolor: 'rgba(66, 66, 66, 0.20)',
                        cursorwidth: "4px",
                        cursorborder: '0px'
                    }).resize();
                }
            }
        }
    };


    setTimeout(function () {
        chat.Message.add();
    }, 1000);
 
    // setTimeout(function () {
    //     // $('#disconnected').modal('show');
    //     $('#call').modal('show');
    // }, 2000);

    // if(userId) {
    //     socketMessage.emit('notifyJoin', userId.userId);
           
    // }
    $(document).on('click', '.layout .content .sidebar-group .sidebar .list-group-item', function () {
        if (jQuery.browser.mobile) {
            $(this).closest('.sidebar-group').removeClass('mobile-open');
        }
    });
   
    $(document).on('click', '.conversation', function() {
        const socket = io.connect('/conversations');
        $('.layout .content .chat .chat-body .messages').html('');
        conversationId = $(this).data('id');
        const userId = $(this).data('userid');
        let conversation_href = $(this).data('href');
        socket.on('connect', function() {
            console.log('join client');
            socket.emit('join', conversationId);
        })
        $(document).on('submit', '.layout .content .chat .chat-footer form', function (e) {
            e.preventDefault();
         
            var input = $(this).find('input[type=text]');
            var message = input.val();
    
            message = $.trim(message);
    
            if (message) {
              
                $.ajax({
                    method: 'POST',
                    url: '/send-message',
                    data: {
                        message,
                        conversationId,
                    },
                })
                // socket.emit('outgoing', { message });
                input.val('');
            } else {
                input.focus();
            }
        });
        socket.on('newMessage', (data) => {
            if(data.data.userId == userId) {
                console.log(userId);
                chat.Message.add(data.data.message, 'outgoing-message');
            }
            else {
              chat.Message.add(data.data.message);
            }
          
            
        })
        // $.ajax({
        //     method: 'GET',
        //     url: conversation_href,
        //     success: function(response) {
        //         console.log(response);
        //     }
        // })
       
    })
   
});

var chat = {
    Message: {
        add: function (message, type) {
            var chat_body = $('.layout .content .chat .chat-body');
            if (chat_body.length > 0) {

                type = type ? type : '';
                message = message ? message : 'Lorem ipsum dolor sit amet.';

                $('.layout .content .chat .chat-body .messages').append('<div class="message-item ' + type + '"><div class="message-content">' + message + '</div><div class="message-action">PM 14:25 ' + (type ? '<i class="ti-check"></i>' : '') + '</div></div>');

                chat_body.scrollTop(chat_body.get(0).scrollHeight, -1).niceScroll({
                    cursorcolor: 'rgba(66, 66, 66, 0.20)',
                    cursorwidth: "4px",
                    cursorborder: '0px'
                }).resize();
            }
        }
    }
};


$(document).ready(function() {
    const socket = io.connect('/conversations');
        // $('.layout .content .chat .chat-body .messages').html('');
        const conversationid = conversationId.id;
        socket.on('connect', function() {
            console.log('join client');
            socket.emit('join', conversationId.id);
            
        })
        if(userId) {
            socketMessage.on('connect', function() {
                socketMessage.emit('notifyJoin', userId.userId);
              })
        }
        $(document).on('submit', '.layout .content .chat .chat-footer form', function (e) {
            e.preventDefault();
         
            var input = $(this).find('input[type=text]');
            var message = input.val();
            message = $.trim(message);
            if (message) {
                $.ajax({
                    method: 'POST',
                    url: '/send-message',
                    data: {
                        message,
                        conversationid,
                    },
                });
                $.ajax({
                    method: 'POST',
                    url: '/notify-message',
                    data: {
                        message,
                        conversationid,
                    }
                })
                // socket.emit('outgoing', { message });
                input.val('');
            } else {
                input.focus();
            }
            socket.on('newMessage', (data) => {
                console.log('m', socket);
                if(data.data.userId == userId.userId) {
                    chat.Message.add(data.data.message, 'outgoing-message');
                }
                else {
                  chat.Message.add(data.data.message);
                }
            })
            socketMessage.on('notifyMessage', (data) => {
                
            })     
        //   console.log('s', socketMessage);
         
        });
       
})