// are not available in the service worker.

$(document).ready(function () {
  const firebaseConfig = {
    apiKey: "AIzaSyDtF3DRzZ9EjA2PaxaVEJUlWtBkuZTeX10",
    authDomain: "phone-19751.firebaseapp.com",
    databaseURL: "https://phone-19751.firebaseio.com",
    projectId: "phone-19751",
    storageBucket: "",
    messagingSenderId: "440843271337",
    appId: "1:440843271337:web:c45663d7e7f263edf8bdb8"
  };
  firebase.initializeApp(firebaseConfig);
  window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container')

  //cloud messag

  const messaging = firebase.messaging();
  Notification.requestPermission().then((permission) => {
    if (permission === 'granted') {
      console.log('Notification permission granted.');
      messaging.getToken().then((currentToken) => {
        if (currentToken) {
         console.log('d', currentToken);
        } else {
          // Show permission request.
          console.log('No Instance ID token available. Request permission to generate one.');
          // Show permission UI.
          // updateUIForPushPermissionRequired();
          // setTokenSentToServer(false);
        }
      }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        
      });
      // TODO(developer): Retrieve an Instance ID token for use with FCM.
      // ...
    } else {
      console.log('Unable to get permission to notify.');
    }
  });
  messaging.onMessage((payload) => {
    console.log('Message received. ', payload);
    // ...
  });
  $('#login-phone-number-1').submit(function (e) {

    var appVerifier = window.recaptchaVerifier;
    e.preventDefault();
    const phoneNumber = $('input[name=phoneNumber]').val();
    let firstName = $('input[name=firstName]').val();
    let lastName = $('input[name=lastName]').val();
    firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
      .then(function (confirmationResult) {
        window.confirmationResult = confirmationResult;
        console.log('Successfully');
        $('#login-form-user').remove();
        $('#login-form').css('display', 'block');
        //Confirm Code 
        $('#login-phone-number-verify').submit(function(e) {
          e.preventDefault();
          let code = $('input[name=code]').val();
          confirmationResult.confirm(code).then(function(result) {
            firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function(idToken) {
              $.ajax({
                method: 'POST',
                data: {
                  phoneNumber,
                  firstName,
                  lastName,
                  idToken,
                },
                url: '/register-phone',
                success: function(response) {
                  console.log(response);
                }
              })
              // Send token to your backend via HTTPS
              // ...
            }).catch(function(error) {
              // Handle error
              console.log(error);
            });
          })
        })
        
      }).catch(function (error) {
        // Error; SMS n
        console.log('failed');
        // ...
      });
  });

  $('#register-email').submit(function (e) {
    e.preventDefault();
    //Register Mail
    var actionCodeSettings = {
      url: "http://localhost:3000/",

      // This must be true.
      handleCodeInApp: true,
    };
    let email = $('input[name=email]').val();
    let password = $('input[name=password]').val();
    let firstName = $('input[name=firstName]').val();
    let lastName = $('input[name=lastName]').val();
    if(email !== '' && password !=='' && firstName !== '' && lastName !== '')
    // Swal.fire(
    //   'Good job!',
    //   'You clicked the button!',
    //   'success'
    // );
    
    firebase.auth().createUserWithEmailAndPassword(email, password).then(function(){
      var user = firebase.auth().currentUser;
      user.sendEmailVerification().then(function (crendential) { 
       firebase.auth().currentUser.getIdToken(true).then((idToken) => {
         // Email sent.
        $.ajax({
          method: 'POST',
          url: '/register-email',
          data: {
            idToken,
            email,
            password,
            firstName,
            lastName,
          }
        })
       })
       
      }).catch(function (error) {
        // An error happened.
        console.log('Errror sending Email');
      });
    }).catch(function (error) {
      console.log('error here', error);
      // Handle Errors here.
      $('#error-message').text(error.message);
      var errorCode = error.code;
      var errorMessage = error.message;
   
      // ...
    });

  });


  $('#login-with-email').submit(function(e) {
    e.preventDefault();
    let email = $('input[name=email]').val();
    let password = $('input[name=password]').val();
      
    firebase.auth().signInWithEmailAndPassword(email, password).then(function(crendential) {
      //Check if user whether verified Email or not
      if(crendential.user.emailVerified) {
        firebase.auth().currentUser.getIdToken(true).then((idToken) => {
          $.ajax({
            method: 'POST',
            url: '/login-email',
            data: {
              idToken,
              email,
            },
            success: function(response) {     
              window.location.href = 'localhost:3000/conversations';
            }
          })
        })
        //handle server

        } else {
          window.alert('Please verified your email before SignIn');
        } 
    })
    .catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      $('#error-message').text(errorMessage);
        console.log(errorMessage)
      // ...
    });
   
  })

  $('button[name=update]').click(function(e) {
    let phoneNumber = $('input[name=addPhone]').val();
    if (phoneNumber !== '') {
      var user = firebase.auth().currentUser;
        if(user) {
          firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function(idToken) {
            $.ajax({
              method: 'POST',
              url: '/update-phone',
              data: {
                uid: user.uid,
                phoneNumber,
                idToken,
              },
              success: function(response) {
                console.log(response);
              }
            })
          });
        }
    }
  })

  $('button[name=add-friend]').click(function(e) {
    let email = $('input[name=email]').val();
    let message = $('input[name=message]').val();
    console.log('dada', email);
    $.ajax({
      method: 'POST',
      url: '/addfriend',
      data: {email: email}
    })
  })

  $('button[name=request-friend]').click(function(e) {
    let friend = document.getElementById('friend-list-request'); 
    friend.innerHTML = "";
    $.ajax({
      method: 'GET',
      url: 'friend-request',
      success: function(response) {
        response.forEach(function(result) {
          let li = document.createElement('li');
              li.className = 'friend-item';
      
              let decline = document.createElement('button');
          decline.className = 'btn btn-danger';
          decline.name = 'declineFriend';
          decline.setAttribute('data-friendId', result.id);
          let accept = document.createElement('button');
          accept.className = 'btn btn-success';
          accept.name = 'acceptFriend';
          accept.setAttribute('data-friendId', result.id);
          let friend = document.getElementById('friend-list-request'); 
          friend.appendChild(li).prepend(`${result.firstName} ${result.lastName}`);
          friend.appendChild(li).prepend(result.phoneNumber);
          friend.appendChild(li).prepend(result.email);
          friend.appendChild(accept).append('Accept');
          friend.appendChild(decline).append('Decline');
        });
      }
    })
  });

  $('button[name=acceptFriend]').click(function(e) {
     $.ajax({
       method: 'POST',
       url: '/friend-accept',
       data: {
          friendId: $(this).attr('data-friendId'),
       },
       success: function(response) {
          // handle successfully accepted

       }
     })
  });

  $('button[name=deleteFriend]').click(function(e) {
    $.ajax({
      method: 'POST',
      url: '/delete-friend',
      data: {
         friendId: $(this).attr('data-friendId'),
      },
      success: function(response) {
         // handle successfully accepted
         
      }
    })
 })

 $('input[name=avatar]').change(function(e) {
  var formData = new FormData();
  const ref = firebase.storage().ref();
  const file = $('#avatar').files[0];
  const name = (+new Date()+ '-'+ file.name);
  const metadata = {
    contentType: file.type
  }
  const task = ref.child(name).put(file, metadata);
  task
  .then(snapshot => snapshot.ref.getDownloadURL())
  .then((url) => {
    console.log(url);
  }).catch(console.log(error));
  
  formData.append('file', e.target.files[0]);
  // console.log('File: ', e.target.files[0]);

  //  $.ajax({
  //   method: 'POST', 
  //   url: 'upload-avatar',
  //   data: formData,
  //   cache: false,
  //   contentType: false,
  //   processData: false,
  //   type: 'POST',
  //   success: function(data) {
  //     console.log(data);
     

  //   }
  //  })
 });

 $('#upload').click(function(e) {
   console.log('wokring');
   let file = $('input[name=avatar]').files;
  $.each($('input[name=avatar]')[0].files)
 });


 
 
 $('#addUsersGroup').keyup(function() {
   console.log('working');
  $.ajax({
    method: 'POST',
    url: '/get-users',
    data: {
      userName: $(this).val(),
    },
    success: function(response) {
      if(response.length) {
        let item = [];
        response.forEach((data, index) => {
         let html = `<li style="border: none; padding: 3px; padding-left: 12px;">
         <div class="row">
            <div class="col-2">
              <div style="height: 40px "><img src="${data.avatar}" style="width: 40px"></div>
            </div>
            <div class="col-8">${data.name}</div>
            <div class="col-2>
            <span class="todo-wrap">
    <input type="checkbox" id="${data.id}" class="checkListAddGroup" name="checkListAddGroup" data-id="${data.id}"/>
    <label for="${data.id }" class="todo check-list">
      <i class="fa fa-check"></i>
    </label>
  </span>
            </div>
          </div>
          </li>`;
          $('.listUserByName').prepend(html);
         })
      }
      else {
        $('.listUserByName').html('');
      }
      var jsonList;
      $('.listUserByName li .checkListAddGroup').click(function() {
        let item = [];
        var checkedVals = $('.checkListAddGroup:checkbox:checked').map(function() {
          return $(this).data('id');
      }).get();
      let index = 0;
      // checkedVals.forEach((data, index) => {
      //   console.log(data);
      // })
      checkedVals.forEach((data, index) => {
        item.push({
          id: data
        })
      }) 
      jsonList = JSON.stringify(checkedVals);
   
      });
      $('button[name="createGroup"]').click(()=> {
       let groupName = $('#group_name').val();
       let description = $('#description').val();
        $.ajax({
          method: 'POST',
          url: '/create-group',
          data: {
            jsonList,
            groupName,
            description,
          },
          success: function(response) {
            console.log(response);
            window.alert('Created Successfully');
            window.location.href('/conversation');
          }
        })
      })
    }
  })
 
 });
//  socketMessage.on('newMessage', (data) => {
//    console.log('data', data);
//  })

});
