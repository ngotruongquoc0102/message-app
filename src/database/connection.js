import config from '../../knexfile';

const env = 'development';
const knex = require('knex')(config[env]);

module.exports = knex;
