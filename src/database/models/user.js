import mongoose from 'mongoose';

const { Schema } = mongoose;

const userSchema = new Schema({
  id:
  {
      type: Number,
      require: 'this field is required',
      unique: true,
  },
  name:
  {
      type: String,
      required: true,
  },
  avatar: String,

});

export default mongoose.model('users', userSchema);
