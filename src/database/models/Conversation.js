import mongoose from 'mongoose';

const { Schema } = mongoose;

const schema = new Schema({
    userId: [{
        type: Schema.Types.ObjectId,
        ref: 'users',
    }],
    groupName: String,
    description: String,
    lastTime: {
        type: Date,
        default: Date.now,
    },
    lastContent: String,
    lastId: {
        type: Number,
    },
},
{
    timestamps: true,
});

export default mongoose.model('conversation', schema);
