import mongoose from 'mongoose';

const { Schema } = mongoose;

const schema = new Schema({
    content: {
        type: String,
        required: 'This field is required',
        trim: true,
    },
    conversationId: {
        type: Schema.Types.ObjectId,
        ref: 'conversations',
    },
    memberId: {
        type: Number,
        required: true,
    },
},
{
    timestamps: true,
});

export default mongoose.model('messages', schema);
