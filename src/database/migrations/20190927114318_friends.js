
exports.up = (knex) => {
  return knex.schema
    .createTable('friends', (table) => {
        table.increments('id').primary();
        table.integer('userId').unsigned();
        table.foreign('userId').references('users.id');
        table.integer('friendId').unsigned();
        table.foreign('friendId').references('users.id');
        table.integer('status').defaultTo(0);
        table.integer('recieved');
    });
};

exports.down = (knex) => {
    return knex.schema
        .dropTable('friends');
};
