exports.up = (knex) => knex.schema.createTable('session', (table) => {
    table.string('sid').primary();
    table.json('sess').notNullable();
    table.timestamp('expire', { precision: 6 }).defaultTo(knex.fn.now());
  });
  exports.down = (knex) => knex.schema.dropTable('session');
