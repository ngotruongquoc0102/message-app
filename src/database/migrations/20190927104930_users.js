
exports.up = (knex) => {
    return knex.schema
      .createTable('users', (table) => {
         table.increments('id').primary();
         table.string('firstName', 255).notNullable();
         table.string('lastName', 255).notNullable();
         table.string('password', 255).notNullable();
         table.string('email', 255).notNullable();
         table.string('phoneNumber', 255);
         table.boolean('verified').defaultTo(false);
         table.timestamp('created_at').defaultTo(knex.fn.now());
         table.timestamp('updated_at').defaultTo(knex.fn.now());
      });
  };
exports.down = (knex) => {
    return knex.schema
      .dropTable('users');
};
