const faker = require('faker');

const data = [
    'Bremont',
    'Bedat & co Blancpain Boucheron',
    'Breguet',
    'Breitling',
    'Bulgari',
    'Carl Bucherer',
    'Cartier',
    'Chaumet Chronographe suisse Concord',
    'Corum',
    'Chopard',
    'De Bethune',
    'De LaCour',
    'Dubey & Shaldenbrand Eberhard & co',
    'F.P Journe',
    'Favre Leuba',
    'Glashütte Original Güblin',
    'H. Moser & cie Harry Winston Hublot',
    'IWC',
    'Jacob & Co',
    'Jaeger LeCoultre Momo design',
    'Mont Blanc',
    'NOMOS',
    'Omega',
    'Panerai',
    'Patek Philippe Pequignet',
    'Perrelet',
    'Porsche design Pro-Hunter Parmigiani Fleurier Piaget',
    'Richard Mille',
    'Rolex',
    'Roger Dubuis',
    'Tag Heuer',
    'Tudor',
    'Ulysse Nardin Vacheron Constantin Wempe',
    'Zenith',
];

const newData = [];
data.forEach((element, i) => {
    newData.push({
        name: element,
        id: i + 1,
        avatar: faker.image.people(),
    });
});
module.exports = newData;
