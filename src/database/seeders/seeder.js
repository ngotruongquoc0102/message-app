const seedr = require('mongoose-seedr');
const path = require('path');

seedr.seed({
    databaseURL: 'mongodb://localhost:27017/messenger-app',
    seed: [
        {
            documents: path.join(__dirname, 'userSeeder.js'),
            collection: 'users',
        },
    ],
});
