import bcrypt from 'bcrypt';
import BaseRepository from '../../../infrastructure/Repositories/BaseRepository';
import knex from '../../../config/database';
import Models from '../../../database/models/index';

class UserRepository extends BaseRepository {
    static repository;

    static getRepository() {
        if(!UserRepository.repository) {
            UserRepository.repository = new UserRepository;
        }

        return UserRepository.repository;
    }

    getTableName() {
        return 'users';
    }

    getTableFriend() {
        return 'friends';
    }
    async signIn(data) {
        const {
           email,
        } = data;
        delete data.idToken;
        const user = await this.getBy(data, 'id');
        await this.update(data, {verified: true});
        // if(user && user.password && bcrypt.compareSync(password, user.password)){
        //     return user;
        // }
        if(user) return user;
        else return;
    }


    async createUser(data) {
        const {
            email,
            firstName,
            lastName,
            password
        } = data;
        delete data.idToken;
        try {
            data.password = bcrypt.hashSync(data.password, bcrypt.genSaltSync(12));
            const addUser = await this.insertData(data);
          
        } catch(e) {
            console.log(e);
           res.status(200);
        }
    }



    async createRoom(data) {
        const {
            title,
        } = data;
       try {
        const room = await knex('rooms').where({'title': title}).first();
        if(room && room.title) {
               console.log('Room already existed');
             res.status(500);
        } else {
             await knex('rooms').insert(data);
             var roomId = await knex('rooms').select('*').where({title: data.title}).first();
        }

       } catch(e) {
           console.log(e);      
             
     }
        return roomId;
    }

    async getRoom(data) {
        const rooms = await knex('rooms').select('*');
       return rooms;
    }
    async addfriendByEmail(data) {
        const userId = data.userId;
        delete data.userId;
        const column = ['id'];
        const friendId = await this.getBy(data, column);
        const action = await this.getByFriend({userId: friendId.id}, ['*']);
       
        if(action) {
            if(action.status == 1) {
                return res.json('exist');
            }
            if(action.status == 0) {
                return res.json('was sent');
            }
        }
        if(friendId) {
            const clause1 = {
                userId,
                friendId: friendId.id,
                status: 0,
                recieved: friendId.id
            }
            const clause2 = {
                userId: friendId.id,
                friendId: userId,
                status: 0,
                recieved: userId,
            }
            await this.createFriend(clause1);
            await this.createFriend(clause2);
        }
        
    }

    async getFriendsRequest(data) {
        const column = ['users.id', 'lastName', 'firstName', 'email', 'phoneNumber'];
        try {
            var friends =  this.listFriends(data, column);
        } catch(error) {
            return;
        }
        return friends;
    }

    async acceptFriend(data) {
        try {
            const attributes = {
                status: 1
            }
            var requestAcceptFriend = this.updateFriend(data, attributes);
        } catch(error) {
           return;
        }
    return requestAcceptFriend;
    }

    async deleteFriendRequest(data) {
        return this.deleteFriend(data);
    }

    createUserMongo(data) {
        return Models.User.create(data);
    }
}

export default UserRepository;
