
import passport from 'passport';
// import FacebookStrategy from 'passport-facebook';
import LocalStrategy from 'passport-local';
import knex from '../../../config/database';

const dotenv = require('dotenv');


dotenv.config({ path: '.env' });

passport.serializeUser((user, done) => {
  done(null, user);
});
passport.deserializeUser((user, done) => {
  done(null, user);
});

/**
 * sign in with facebook
 * */
// passport.use(new FacebookStrategy({
//     clientID: process.env.FACEBOOK_ID,
//     clientSecret: process.env.FACEBOOK_SECRET,
//     callbackURL: 'https://e7f1a6be.ngrok.io/admin/auth/facebook/callback',
//     profileFields: ['name', 'email', 'link', 'locale', 'timezone', 'gender'],
//     passReqToCallBack: true,
// }, (accessToken, refreshToken, profile, done) => (profile ? done(null, profile, accessToken) : done(null))));
// passport.use(new LocalStrategy({
//   async function(username, password, done) {
// 
//       try {
//             const user = await knex('user').select('*').where('username', '=', 'json');
//             if (!user) {
//                 return done(null, false, { message: 'incorrect username or password' });
//             }
//       } catch (err) {
//           return done(err);
//       };
      
//    }
// }));

export default passport;
