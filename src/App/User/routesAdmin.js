import express from 'express';
import UserAuthorize from './middleware/AdminUserAuthorize';

const router = express.Router();
const userAuthorize = new UserAuthorize();

router.get('/create', userAuthorize.handleAuthorization('create'), (req, res) => {
    console.log('working');
});

export default router;
