import UserRepository from '../Repositories/UserRepository';
import jwt from 'jsonwebtoken';
import { JWT_SECRET, EXPIRATION } from '../../../config/jsonToken';
import knex from '../../../config/database';
import * as admin from 'firebase-admin';
import serviceAccount from '../../../config/token.json';

class UserService {
  static userService;
    constructor() {
      this.userRepository = UserRepository.getRepository();
    }

   async signIn(data) {
       let user = await this.userRepository.signIn(data);
       if (user) {
        //  user = this.getToken({userId: data.id, firstName: data.firstName, lastName: data.lastName});
         return user;
       }
     
    }

    async getListContact(data) {
      const listContact = await this.userRepository.createUserMongo(data);
      if(listContact) {
        return listContact;
      }
    }
    async getFriendsRequest(data) {
      const clause = {
        userId: data.userId,
        status: 0,
      }
      return await this.userRepository.getFriendsRequest(clause);
    }
    async register(data) {
      const createUser = await this.userRepository.createUserMongo(data);
     const user = await this.userRepository.createUser(data);
    }

    async updatePhoneNumber(data) {
      console.log('workingadwa');
      admin.auth().updateUser(data.uid, {
        phoneNumber: data.phoneNumber
      })
      .then((userRecord) => {
        console.log('successfully updated user', userRecord.toJSON());
      })
      .catch((error) => {
        console.log('Error updating', error);
      })
    }
    async createRoom(data, res) {
      const add = await this.userRepository.createRoom(data);
      res.json(add);
    }

    async showRoom(data) {
      const rooms = await knex('rooms').select('*').first();
      return rooms;
    }

    async getToken(data) {
      console.log('successfully');
       return jwt.sign({user: data}, JWT_SECRET, {expiresIn: EXPIRATION});
    }

   async addfriendByEmail(data) {
      const add = await this.userRepository.addfriendByEmail(data);
    
   }

    async addfriendByPhone(data) {
      
    }

    async listFriendRequest(data) {
      const clause = {
        status: 0,
        userId: data.id
      }
      const listRequest = await this.userRepository.getFriendsRequest(clause);
       return listRequest
    }

    async acceptFriend(data) {
        return await this.userRepository.acceptFriend(data);
    }

    async deleteFriend(data) {
      return await this.userRepository.deleteFriendRequest(data);
    }
   static getService() {
    if (!UserService.userService) {
      UserService.userService = new UserService();
    
    }

    return UserService.userService;
}


}
export default UserService;