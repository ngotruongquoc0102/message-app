import express from 'express';

import AdminUserController from './controller/AdminUserController';
import AuthMiddleware from './middleware/AuthMiddleware';
import MessageController from '../Messages/Controller/messageController';
import handleHelper from '../../infrastructure/Helpers/LocalHelpers';
import flash from '../../infrastructure/Helpers/localVariable';

const adminUserController = new AdminUserController();
const messageController = new MessageController();
const authMiddleware = new AuthMiddleware();

const router = express.Router();
// connection IO

router.use(flash.flash_middleware);
router.use((req, res, next) => {
    handleHelper(res);
    next();
});
// router.get('/sign-in', adminUserController.callMethod('showSignInForm'));
// router.post('/sign-in', adminUserController.callMethod('signIn'));
router.post('/sign-in', adminUserController.callMethod('signIn'));
router.get('/rooms', adminUserController.callMethod('getRooms'));
router.post('/register', adminUserController.callMethod('add'));
router.post('/rooms/create', adminUserController.callMethod('create'));
router.get('/chat/:room', adminUserController.callMethod('showRoomsChat'));
router.get('/api', adminUserController.callMethod('getUser'));
router.get('/chat', (req, res) => {
    res.render('app/client/index.ejs');
});
 router.get('/login', authMiddleware.checkIfNotVerifiedAuthentication, adminUserController.callMethod('showLoginForm'));
router.get('/register', (req, res) => res.render('app/register'));

router.get('/reset-password', (req, res) => res.render('app/reset-password'));

router.get('/', (req, res) => res.redirect('/conversations'));


router.get('/t/:id', authMiddleware.checkIfVerifiedAuthentication, adminUserController.callMethod('showHomeIndex'));
router.get('/conversations', authMiddleware.checkIfVerifiedAuthentication, messageController.callMethod('renderConversation'));
router.get('/send-phone', (req, res) => res.render('app/loginPhoneNumber'));

router.post('/register-phone', authMiddleware.verifyToken, adminUserController.callMethod('postRegisterPhone'));

router.post('/update-phone', authMiddleware.verifyToken, adminUserController.callMethod('updatePhone'));
router.post('/register-email', authMiddleware.verifyToken, adminUserController.callMethod('postRegisterMail'));
router.post('/login-email', authMiddleware.verifyToken, adminUserController.callMethod('signIn'));
router.get('/logout', adminUserController.callMethod('logout'));
router.post('/addfriend', adminUserController.callMethod('addfriend'));
router.get('/friend-request', adminUserController.callMethod('friendRequest'));
router.post('/friend-accept', adminUserController.callMethod('acceptFriend'));
router.post('/delete-friend', adminUserController.callMethod('deleteFriend'));
router.get('/friend-list', adminUserController.callMethod('listFriend'));

export default router;
