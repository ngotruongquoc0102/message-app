import * as admin from 'firebase-admin';
import UserController from './UserController';

class AdminUserController extends UserController {
  

  verifyToken(token) {
    // idToken comes from the client app
    this.admin.auth().verifyIdToken(token)
      .then((decodedToken) => {}).catch((error) => {
        console.log(error);
        // Handle error
      });
  }
}
export default AdminUserController;
