/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-named-as-default-member */
import jwt from 'jsonwebtoken';

import baseController from '../../../infrastructure/Controller/BaseController';
import UserService from '../services/UserService';
import MessageService from '../../Messages/Services/MessageService';
import MessageController from '../../Messages/Controller/messageController';
import { JWT_SECRET, EXPIRATION, TOKEN } from '../../../config/jsonToken';

const messageController = new MessageController();
class UserController extends baseController {
    constructor() {
      super();
      this.userService = UserService.getService();
      this.messageService = MessageService.getService();
    }
    // eslint-disable-next-line class-methods-use-this

    showRegisterByphone(req, res) {
      return res.render('app/loginPhoneNumber');
    }

    postRegisterPhone(req, res) {
      this.userService.register(req.body);
      return res.redirect('/login');
    }

 async showHomeIndex(req, res) {
      const data = {
        userId: req.session.cUser,
       };
      const getFriendsRequest = await this.userService.getFriendsRequest(data);
      const ObjectId = await this.messageService.getObjectId(data.userId);
      const list = await this.messageService.getListConversation(ObjectId);
      const LastName = await this.messageService.getObjectId(list[0].lastId);
      const conversationId = {
        id: req.session.conversationId,
      };
      const messageId = req.params.id;
      const content = await this.messageService.renderMessage(conversationId.id);
      return res.render('app/conversation/index', { friendsRequest: getFriendsRequest, conversations: list, userId: data.userId, conversationId: JSON.stringify(conversationId), dataId: JSON.stringify(data), content: JSON.stringify(content), contents: content, LastName });
    }

    showLoginForm(req, res) {
      return res.render('app/login');
    }

    logout(req, res) {
      req.session.destroy();
      return res.redirect('/login');

    }

    postRegisterMail(req, res) {
      this.userService.register(req.body);
      res.json('successfully');
    }

    async friendRequest(req, res) {
      const userId = req.session.cUser;
      const friendsRequest = await this.userService.listFriendRequest({ id: userId });
      res.json(friendsRequest);
    }

    acceptFriend(req, res) {
      const clause1 = {
          userId: req.session.cUser,
          friendId: req.body.friendId,
      };

      const clause2 = {
        userId: req.body.friendId,
        friendId: req.session.cUser,
    };
      const acceptFriend1 = this.userService.acceptFriend(clause1);
      const acceptFriend2 = this.userService.acceptFriend(clause2);
      if (acceptFriend1 && acceptFriend2) {
       return res.json('Accepted invation');
      }
    }

    deleteFriend(req, res) {
      const clause1 = {
        userId: req.session.cUser,
        friendId: req.body.friendId,
    };

    const clause2 = {
      userId: req.body.friendId,
      friendId: req.session.cUser,
  };
  const deleteUserFriend = this.userService.deleteFriend(clause1);
  const deleteFriendUser = this.userService.deleteFriend(clause2);
      if (deleteUserFriend && deleteFriendUser) {
        return res.json('Deleted Successfully');
      }
    }

    updatePhone(req, res) {
      this.userService.updatePhoneNumber(req.body);
      res.json('successfully');
    }

    // eslint-disable-next-line class-methods-use-this
    showSignInForm(req, res) {
        return res.render('app/conversation/index.pug', {
          title: 'Sign in',
        });
      }

    uploadAvatarProfile(req, res) {
      console.log(req.file);
     }

     async signIn(req, res) {
       const user = await this.userService.signIn(req.body);
       if (!user) {
          res.redirect('/login');
       }
       req.session.cUser = user.id;
       req.session.save();
       res.json(user);
      }

      // async getUser(req, res) {
      //  try {
      //     const decoded = await jwt.verify(TOKEN, JWT_SECRET);
      //     console.log(decoded);
      //  } catch (e) {
      //    console.log(e);
      //  }
      // }

      async add(req, res) {
          return this.userService.register(req.body);
      }

    async findNearestConversation(req, res) {
      
    }

      async create(req, res, next) {
          const addRoom = this.userService.createRoom(req.body, res);
        }

      showRoomsChat(req, res) {
        return res.render('app/client/chatRoom.ejs', { title: 'chatRoom' });
      }

      addfriend(req, res) {
        const data = {
            email: req.body.email,
            userId: req.session.cUser,
        };
          this.userService.addfriendByEmail(data);
          return res.json('successfully');
        }

}


export default UserController;
