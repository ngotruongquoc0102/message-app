import * as admin from 'firebase-admin';
import BaseAuthorize from '../../../infrastructure/middleware/BaseAuthorize';
import serviceAccount from '../../../config/token.json';

class UserAuthorize extends BaseAuthorize {
    constructor() {
        super();
        console.log('working out');
        admin.initializeApp({
          credential: admin.credential.cert(serviceAccount),
          databaseURL: 'https://phone-19751.firebaseio.com',
        });
      }

verifyToken(req, res, next) {
    admin.auth().verifyIdToken(req.body.idToken)
    .then((decodedToken) => {
        // console.log(decodedToken);
        return next();
    }).catch((error) => {
        res.json(error);
    });
 } 
 checkIfNotVerifiedAuthentication(req, res, next) {
     if(!req.session.cUser) {
         return next();
     }
     return res.redirect('/');
 }
checkIfVerifiedAuthentication(req, res, next) {
    if(req.session.cUser) {

        return next();
    }
    return res.redirect('/login');
}
}

export default UserAuthorize;