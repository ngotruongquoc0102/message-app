import BaseAuthorize from '../../../infrastructure/middleware/BaseAuthorize';

class AdminUserAuthorize extends BaseAuthorize {
    defineRoutes() {
        this.routes = {
          listUsers: 'ListUsers',
          listStaffs: 'ListStaffs',
          create: 'ListStaffs',
          show: 'Show',
          edit: 'Edit',
          delete: 'Delete',
        };
      }

      authorizeListStaffs(req, res, next) {
        
      }

}
export default AdminUserAuthorize;
