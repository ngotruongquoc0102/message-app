
import models from '../../../database/models/index';
import { Model } from 'mongoose';

class MessageRepository {
    static repository;

    static getRepository() {
        if(!MessageRepository.repository) {
            MessageRepository.repository = new MessageRepository();
        }
        return MessageRepository.repository;
    }

    findUserByName(data) {
       return models.User.find({'name': {'$regex': data.userName}}).limit(10);
    }
    
    createGroupByName(data) {
        return models.Conversation.create(data);
    }

    getById(id) {
        return models.User.find({
            id: id,
        }).select('_id name');
    }

    getListConversation(id) {
        return models.Conversation.find({ userId: { $elemMatch: { $eq: id } } }).select('description groupName _id lastId lastContent lastTime').populate('userId', 'name avatar id')
    }

    getConversationById(id) {
        return models.Conversation.find({ _id: id});
    }

    createMessage(data) {
       return models.Message.create(data);
    }

    updateConversation(conversationId, data) {
        console.log('update', data);
        console.log(conversationId);
        return models.Conversation.findById(conversationId).updateMany(data);
    }

    getNearestConversation(data) {
        const Id = data[0]._id;
        return models.Conversation.find({ userId: { $elemMatch: { $eq: Id } } }).sort('-updatedAt').limit(1);
    }

    getContentByConversationId(id) {
        return models.Message.find({ conversationId: id }).sort('createdAt').select('content memberId createdAt');
    }

    getMemberByConversationId(conversationId) {
        return 
    }
}

export default MessageRepository;

