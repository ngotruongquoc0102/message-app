import express from 'express';
import MessageController from './Controller/messageController';
//import authMiddleware from '../../infrastructure/middleware/BaseAuthorize';
const messageController = new MessageController();

const router = express.Router();

router.post('/get-users', messageController.callMethod('renderUser'));
router.post('/create-group', messageController.callMethod('createGroup'));
// router.get('/get-users', imageController.callMethod('renderUser'));
//router.get('/', messageController.callMethod('renderConversation'));
router.post('/send-message', messageController.callMethod('handleMessage'));
router.post('/notify-message', messageController.callMethod('notifyMessageConversation'));
export default router;
