
import MessageRepository from '../Repositories/MessageRepository';
import Conversation from '../../../database/models/Conversation';
import getSlug from 'speakingurl';
import { promises } from 'dns';

class MessageService {
 static messageService;
 static io;
 static UserIo;
constructor() {
    this.messageRepository = MessageRepository.getRepository();
}
static getService() {
    if(!this.messageService) {
        this.messageService = new MessageService();
    }
return this.messageService;
}


async findUserByName(data) {
    return await this.messageRepository.findUserByName(data);
}

async getObjectId(id) {
    return await this.messageRepository.getById(id);
}

async getListConversation(ObjectId) {
   return await this.messageRepository.getListConversation(ObjectId[0]._id);
}
async createGroup(data, group) {
        let item = [];
        const users = await Promise.all(data.map(element => this.messageRepository.getById(element)));
        // user.forEach((element) => {
        //     console.log(element[0]);
        // })
        users.forEach((user) => {
            // console.log(user[0]._id);
          item.push( user[0]._id);  
        });
        const dataUser = {
            userId: item,
            description: group.description,
            groupName: group.groupName,
        }
        dataUser.slug = getSlug(`${dataUser.groupName}-${Date.now()}`)
        const createGroup = await this.messageRepository.createGroupByName(dataUser);
}

async getNearestConversation(id) {
    return await this.messageRepository.getNearestConversation(id);
}

async postData(data, userId) {
  const contact = await this.messageRepository.getConversationById(data.conversationid);
  const newData = {
    content: data.message,
    memberId: data.userId,
    conversationId: data.conversationid,
};

  const [ content ] = await Promise.all([
      this.messageRepository.createMessage(newData),
      this.messageRepository.updateConversation(data.conversationid, {
          lastContent: data.message, lastId: data.userId, lastTime: Date.now(), 
      }),
  ]);

  await  MessageService.io.to(data.conversationid).emit('newMessage', { data });
}
async renderMessage(conversationId) {
    return await this.messageRepository.getContentByConversationId(conversationId);
}

async notifyMessageConversation(data) {
    console.log('server:', data);
     MessageService.UserIo.to(data.userId).emit('notifyMessage', { data });
}

async getMemberByConversationId(data) {
    console.log(data);
 return await this.messageRepository.getMemberByConversationId(data);
}
}
export default MessageService;