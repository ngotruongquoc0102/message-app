import BaseController from '../../../infrastructure/Controller/BaseController';
import MessageService from '../Services/MessageService';


class messageController extends BaseController {
    constructor() {
        super();
        this.messageService = MessageService.getService();
    }

    getRooms(req, res, next) {
        return res.render('app/conversation/room');
    }

   async renderUser(req, res) {
        const data = req.body;
        const response = await this.messageService.findUserByName(data);
        res.json(response);
    }

    async createGroup(req, res) {
        const data = req.body;
        const newData = JSON.parse(data.jsonList);
        const userId = req.session.cUser;
        newData.push(
            userId,
        );

        return res.json(await this.messageService.createGroup(newData, data));
    }

    async getListConversation(req, res) {
        const userId = req.session.cUser;
       const ObjecId = await this.messageService.getObjectId(userId);
    }

    async renderMessage(req, res) {
       const messageId = req.params.id;
       const userId = req.session.cUser;
       const objetUserId = await this.messageService.getObjectId(userId);
       //const content = await this.messageService.renderMessage()
       //console.log(content);
      // const getFriendsRequest = await this.userService.getFriendsRequest(data);
       //return res.render('app/conversation/index');
    }

    async renderConversation(req, res) {
       
        const userId = req.session.cUser;
        const objetUserId = await this.messageService.getObjectId(userId);
        const conversation = await this.messageService.getNearestConversation(objetUserId);
        req.session.conversationId = conversation[0]._id;
        res.redirect(`/t/${conversation[0]._id}`);
    }

    async handleMessage(req, res) {
        const data = req.body;
        data.userId = req.session.cUser;
        const createMessage = this.messageService.postData(data);
        }

        async notifyMessageConversation(req, res) {
        const data = {
            conversationId: req.body.conversationid,
            message: req.body.message,
            userId: req.session.cUser,
        };
        const membersRoom = this.messageService.getMemberByConversationId(data.conversationId);
        const updateConversation = this.messageService.notifyMessageConversation(data);
    }
}
export default messageController;
