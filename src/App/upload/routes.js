import express from 'express';
import multer from 'multer';
import ImageController from './controller/uploadFileController';

const imageController = new ImageController();
const router = express.Router();

const upload = multer({ dest: 'public/tmp/images' });

router.post('/upload-avatar', upload.single('file'), imageController.callMethod('uploadProfile'));
// router.get('/get-users', imageController.callMethod('renderUser'));
export default router;
