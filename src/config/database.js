import primaryKnex from 'knex';

const knex = primaryKnex({
    client: 'pg',
    connection: process.env.DB_URL,
    asyncStackTraces: true,
});

export default knex;
