import socket from 'socket.io';
import MessageService from '../App/Messages/Services/MessageService';

export default function (server) {
    const io = socket(server);
    MessageService.io = io.of('/conversations').on('connection', (client) => {
        client.on('join', (id) => {
            console.log('join', id);
            client.join(id);
        });
        
        // client.on('outgoing', (data) => {
        //     console.log(data);
        //     client.broadcast.emit('incomming', { data });
        // });
    });
    MessageService.UserIo = io.of('/notifyMessage').on('connection', (client) => {
        console.log('hi server 1');
        client.on('notifyJoin', (id) => {
            console.log('join user', id);
            client.join(id);
        });
    })
}