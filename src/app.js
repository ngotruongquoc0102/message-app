import createError from 'http-errors';
import 'dotenv/config';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import http from 'http';
import bodyParser from 'body-parser';
import Session from 'express-session';
import ConnectPg from 'connect-pg-simple';
import mongoose from 'mongoose';
import flash from 'connect-flash';
// import logger from 'morgan';
import configSocket from './config/socket';


import indexRouter from './routes/web';

const app = express();
const server = http.createServer(app);
configSocket(server);

// view engine setup
app.set('views', path.join(__dirname, 'resources/views'));
app.set('view engine', 'pug');

//  app.use(logger('dev'));
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());

const PostgreSqlStore = ConnectPg(Session);
const session = Session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  name: 'messenger-app.session',
  cookie: {
    maxAge: parseInt(process.env.SESSION_LIFETIME, 10) * 1000,
  },
  store: new PostgreSqlStore({
    conString: process.env.DB_URL,
  }),
});

mongoose.Promise = global.Promise;
mongoose.connect(process.env.DB_MONGODB_URL, { useNewUrlParser: true });
app.use(session);

app.use('/', indexRouter);
app.use((req, res, next) => {
  res.locals.conversationId = req.conversationId;
  next(createError(404));
});

// catch 404 and forward to error handler

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

export default app;
